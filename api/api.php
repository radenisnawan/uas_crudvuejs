<?php

	header("Content-type: application/json");

	// Koneksi database
	$conn = new mysqli("localhost", "root", "", "uasvuejs");
	if ($conn->connect_error) {
		die("Koneksi database gagal!");
	}
	$res = array('error' => false);

	// Read data dari database uasvuejs
	$action = 'read';

	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	}

	if ($action == 'read') {
		$result = $conn->query("SELECT * FROM `mahasiswa`");
		$mahasiswa  = array();

		while ($row = $result->fetch_assoc()) {
			array_push($mahasiswa, $row);
		}
		$res['mahasiswa'] = $mahasiswa;
	}

	// Insert data ke dalam database uasvuejs
	if ($action == 'create') {
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nim   = $_POST['nim'];

		$result = $conn->query("INSERT INTO `mahasiswa` (`nama`, `email`, `nim`) VALUES('$nama', '$email', '$nim')");

		if ($result) {
			$res['message'] = "Berhasil menambahkan mahasiswa";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal menambahkan mahasiswa";
		}
	}

	// Update data
	if ($action == 'update') {
		$id   = $_POST['id'];
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nim   = $_POST['nim'];


		$result = $conn->query("UPDATE `mahasiswa` SET `nama`='$nama', `email`='$email', `nim`='$nim' WHERE `id`='$id'");

		if ($result) {
			$res['message'] = "Berhasil update mahasiswa";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal update mahasiswa";
 		}
	}

	// Delete data
	if ($action == 'delete') {
		$id       = $_POST['id'];
		$nama = $_POST['nama'];
		$email    = $_POST['email'];
		$nim   = $_POST['nim'];

		$result = $conn->query("DELETE FROM `mahasiswa` WHERE `id`='$id'");

		if ($result) {
			$res['message'] = "Berhasil delete mahasiswa";
		} else {
			$res['error']   = true;
			$res['message'] = "Gagal delete mahasiswa";
		}
	}

	// Tutup koneksi database
	$conn->close();
	
	echo json_encode($res);
	die();
?>