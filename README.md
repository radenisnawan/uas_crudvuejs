Project ini adalah Ujian Akhir Semester pada mata kuliah Pemrograman Web Mobile, kelas IF-5, prodi Informatika.

## Anggota kelompok: 

1. Gunawan Saputra (NIM 195410215 / kelas IF-5) 
2. Ahmad Shoffian (NIM 195410219 / kelas IF-5)
3. Satria Dwi Hartanto (NIM 195410229 / kelas IF-5)
4. Reza Dian Saputro (NIM 195410230 / kelas IF-5)
5. Raden Isnawan Argi Aryasatya (NIM 195410257 / kelas IF-5)


## Cara menggunakan

1. Buat database
2. import file uasvuejs.sql
3. ganti koneksi database pada file `<path_project>/api/api.php` pada baris #6 sesuai database Anda
4. ganti URL pada file `<path_project>/js/app.php` misalnya pada code `'http://localhost/uasvuejs/api/api.php?action=read'`. Ganti code pada baris #22, #36, #52, #68. Ganti sesuai dengan path project Anda
5. jalankan dengan `localhost/<folder_project>` misalnya `localhost/uasvuejs` pada browser

## Tampilan struktur project di komputer kami

```
+--folder "uasvuejs"
   |- index.php
   |- style.css
         |
    +--folder "api"
    |    |
    |    +-- api.php
    |
    +--folder "css"
    |    |
    |    +-- file-file bootstrap css
    |
    +--folder "images"
    |    |
    |    +-- logo.png
    |    | 
    |    +-- UTDI-logo.png
    |
    +--folder "js"
    |    |
    |    +-- app.js
    |    | 
    |    +-- jquery.js
    |    |
    |    +-- file-file bootstrap js

```
Keterangan:
Kami membuat folder `uasvuejs` di dalam htdocs. Kemudian di dalam folder tersebut ada 2 file (index.php & style.css) dan 4 folder (`api`,`css`,`images`,`js`).
